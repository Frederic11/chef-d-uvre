<?php

namespace App\Form;

use App\Entity\Urgent;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class UrgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add( 'date' )
            // date et user sont désactivé, pour demander au builder de ne pas construire de rubrique 
            // car ces variables sont enregistrés automtiquements
            ->add('motif', ChoiceType::class, [
                'choices' => ['Retard' => 'Retard', 'Abscence' => 'Abscence'],
            ])
            ->add('timedelay', ChoiceType::class, [
                'choices' => ['5' => 5, '10' => 10, '20'=>20],
            ])
            ->add('content',TextareaType::class)
            //->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Urgent::class,
        ]);
    }
}
