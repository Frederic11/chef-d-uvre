<?php

namespace App\Form;

use App\Entity\Team;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;





class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            //->add('roles'[0])
            
            ->add('password')
            ->add('activity',   ChoiceType::class, [
                'choices' => ['Manager' => 'Manager', 'Conseiller' => 'Conseiller'],
            ])
            ->add('name')
            ->add('firstname')
            ->add('mail')
            ->add('mobile')
            ->add('location')
            //->add('team')
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
