<?php

namespace App\DataFixtures;

use App\Entity\Team;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;



class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        
        for ($i=1; $i < 5; $i++) { 
            $user = new User();
            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setUsername('user'.$i)
            ->setPassword($hashedPassword)
            ->setRoles(['ROLE_USER'])
            ->setActivity('Conseiller')
            ->setName('Cabrel')
            ->setFirstname('Francis')
            ->setMail('ladamedehautesavoir@jiraidormir.chez')
            ->setMobile('08080808')
            ->setLocation('plateau C - place '.$i)

            ;



            
            $manager->persist($user);
        }
        

                
        for ($i=1; $i < 5; $i++) { 
            $team = new Team();
            

            $team->setState('green')
            ;



            
            $manager->persist($team);
        }

        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, 'admin');

        $user->setUsername('admin')
        ->setPassword($hashedPassword)
        ->setRoles(['ROLE_ADMIN'])
        ->setActivity('Manager')
        ->setName('Cabrel')
        ->setFirstname('Francis')
        ->setMail('ladamedehautesavoir@jiraidormir.chez')
        ->setMobile('08080808')
        ->setLocation('plateau C - place '.$i)
        ;
        $manager->persist($user);




        $manager->flush();
    }
}
