<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\TeamRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AuthController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(AuthenticationUtils $utils)
    { 
        $username = $utils->getLastUsername();
        $errors = $utils->getLastAuthenticationError();
        return $this->render('auth/index.html.twig', [
            'errors' => $errors,
            'username' => $username
        ]);
    }

    /**
     * @Route("admin/register", name="register")
     */
    public function forme(Request $request, EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder)
    {
        //request pour faire une requete
        //on crait une nouvelle instance user en appelant le user entity
        $newUser = new User;
        //pour le moment c'est vide puis on fait un formulaire  pour hydrater l'objet
        //on crait un formulaire qui s'appelle form de class usertype qui va hydrater user
        $form = $this->createForm(UserType::class, $newUser);

        $form->handleRequest($request);
        $user = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            //récuperer mot de passe et encoder
            $hashedPassword = $encoder->encodePassword($newUser, $newUser->getPassword());
            $newUser->setPassword($hashedPassword);
            $manager->persist($newUser);
            $manager->flush();
        }

        //on crait la vu dans le twig avec createview
        return $this->render('auth/register.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/modify/{id}", name="modify")
     */
    public function modify(User $user, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {

        //partie identique à la création du user, sauf qu'on ne crait pas une nouvelle instance, entité utilisé
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashedPassword);
            $manager->persist($user);
            $manager->flush();
            $manager->persist($user);
            $manager->flush();
        }

        //on crait la vu dans le twig avec createview
        return $this->render('auth/modify.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/team", name="team")
     */
    public function register(UserRepository $repo, TeamRepository $team)
    {
        // nous devons récuperer ici tout les instances
        //appel au repository qui donne des indications sur la manipulation de
        // l'objet et le findall pour tous les appelés
        // retourne dans html.twig la valeur users qui sera égal a la valeur repo qui grace à la function findall comprendra

        //toute les instance

        $user = $this->getUser();


        return $this->render('auth/team.html.twig', [
            'user' => $user,
            'teams' => $team->findall(),
            'users' => $repo->findBy(array(), array('name' => 'ASC'))
        ]);
    }


    /**
     * @Route("/efface/{id}", name="efface")
     */
    public function efface(User $user, Request $request, EntityManagerInterface $manager)
    {
        $manager->remove($user);
        $manager->flush();

        //on crait la vu dans le twig avec createview
        return $this->render('auth/efface.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     *  @Route("/user/{user}/team/{team}", name="userteam")
     */
    public function attache(User $user, Team $team, EntityManagerInterface $manager)
    {
        //route du dessus a récupere du template l'id des entity qu'on souhaite
        //on récupere les fonctions définit dans le user pour pouvoir rattacher
        $user->addTeam($team);
        $manager->persist($user);
        $manager->flush();
        $teams = 0;
        $user->getTeams();


        return $this->render('auth/userteam.html.twig', [
            'user' => $user,
            'team' => $team,
            'teams' => $teams,
        ]);
    }

    /**
     *  @Route("/erase/user/{user}/team/{team}", name="erase")
     */
    public function erase(User $user, Team $team, EntityManagerInterface $manager)
    {   //meme chose que pour la fonction précédente, simplement on remplace addTeam par RemoveTeam
        //route du dessus a récupere du template l'id des entity qu'on souhaite
        //on récupere les fonctions définit dans le user pour pouvoir rattacher
          $user->removeTeam($team);
          $manager->persist($user);
          $manager->flush();
          $teams=0;
          $user->getTeams();


        return $this->render('team/erase.html.twig', [
            'user' => $user,
            'team' => $team,
            // 'teams'=> $teams,
        ]);
    }
}
