<?php

namespace App\Controller;

use App\Entity\Team;
use App\Form\TeamType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;




class TeamController extends AbstractController
{
    /**
     * @Route("/modifyTeam/{id}", name="modifyTeam")
     */
    public function index(UserRepository $user, Team $team, Request $request, EntityManagerInterface $manager)
    {
        
        $form = $this->createForm(TeamType::class, $team);

        $form->handleRequest($request);

        $user=$this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($team);
            $manager->flush();
        }

        return $this->render('team/index.html.twig', [
            'user'=>$user,
            'controller_name' => 'TeamController',
            'form' => $form->createView(),
        ]);
    }
}
