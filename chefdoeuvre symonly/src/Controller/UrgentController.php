<?php

namespace App\Controller;

use App\Entity\Urgent;
use App\Entity\User;
use App\Form\UrgentType;
use App\Repository\UrgentRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;





class UrgentController extends AbstractController
{
    /**
     * @Route("urgent/", name="urgent")
     */
    public function index(UserRepository $user, UrgentRepository $urgent, Request $request, EntityManagerInterface $manager)
    { //getuser pour avoir l'id de la personne qui utilise le site
        $user = $this->getUser();
        setlocale(LC_TIME, 'fra_fra');
        $ddate = strftime('%d/%m/%y');

        $urgent = new Urgent;
        //pour le momen c'est vide mais fabriquand un formulire pour hydrater l'objet
        //on crait un formulaire qui s'appelle form de class usertype qui va hydrater user
        $form = $this->createForm(UrgentType::class, $urgent);




        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $urgent->setDate(
                new DateTime()
            );
            $urgent->setUser($user);
            $manager->persist($urgent);
            $manager->flush();
        }



        return $this->render('urgent/index.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'ddate' => $ddate
        ]);
    }

    /**
     * @Route("/urgent/fetch", methods="GET" )
     */
    public function delayandabscence(UrgentRepository $urgent)
    {
        // pb circulaire, le programme tourne en rond avec le urgent qui a un user qui a un urgent
        //solution proposé pour bloqué cela 
        //    serializer:
        // circular_reference_handler: App\Serializer\CircularHandler
        //ajouté dans sécurity yaml
        //puis creation du fichier circularHandle.php

        return $this->json($urgent->findAll());
    }
}
