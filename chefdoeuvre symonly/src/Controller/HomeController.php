<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\User;
use App\Repository\TeamRepository;
use App\Repository\UrgentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(TeamRepository $teams )

    {
        $user = $this->getUser();
        $user->getTeams();
        //getuser a comme fonction de récuperer l'instance du user connecté
        return $this->render('home/index.html.twig', [
            'user' =>    $user, 'teams' => $teams
        ]);
    }

    
}
