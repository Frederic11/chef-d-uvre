<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Repository\UrgentRepository;
use App\Repository\UserRepository;
use DateTime;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;




class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="news")
     */
    public function newForm(UserRepository $user, UrgentRepository $urgent, Request $request, EntityManagerInterface $manager)
    {
        //getUser permet de conserver l'id de l'utilisateur
        $user = $this->getUser();
        //setlocale définit la localisation 
        setlocale(LC_TIME, 'fra_fra');
        //on récupère la date d'aujourd'hui et on formate la date
        $ddate = strftime('%d/%m/%y');

        //ici on crait une nouvelle instante de News
        $news = new News;
        //pour le moment c'est vide on fabrique  un formulire pour hydrater l'objet
        //on crait un formulaire qui s'appelle form de class usertype qui va hydrater user
        //form sera un formulaire de classe NewsType
        $form = $this->createForm(NewsType::class, $news);



        //handlrequest permet de conserver les donnés du form
        $form->handleRequest($request);

        //on vérifie que les donnés soit soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            //on donne comme valeur date, la date d'aujourd'hui.
            //$news->setDate(new DateTime());
            $news->setDate(new DateTime());
            //on donne la valeur du user connecté
            $news->setUser($user);
            //on fait persister les informations et on envoie en base de donné (flush)
            $manager->persist($news);
            $manager->flush();
        }
        return $this->render('news/index.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'ddate' => $ddate

        ]);
    }

    //une fois le formulaire fait, il faut pouvoir envoyer info, ici sous forme de fichier json

    /**
     * @Route("/new", methods="GET" )
     */
    public function delayandabscence(NewsRepository $news)
    {
        // on récupère la fonction findall qui va récupérer toute les instances News

        return $this->json($news->findAll());
    }
}
