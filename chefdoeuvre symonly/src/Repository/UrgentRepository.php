<?php

namespace App\Repository;

use App\Entity\Urgent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Urgent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Urgent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Urgent[]    findAll()
 * @method Urgent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Urgent::class);
    }

    // /**
    //  * @return Urgent[] Returns an array of Urgent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Urgent
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
