<?php

namespace App\Tests;

use App\Controller\NewsController;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;



class NewsControllerTest extends WebTestCase
{

    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();   
    }

    public function testaddNews()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
            $form = $crawler->selectButton('Login')->form();
            $form['_username'] = 'admin';
            $form['_password'] = 'admin';
            $client->submit($form);

        $crawler = $client->request('GET', '/news');
        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Ajouter')->form();
        $form['news[content]'] = 'test';
        $client->submit($form);
        // $this->assertResponseRedirects('/news');
        $repo = static::$container->get('App\Repository\NewsRepository');
        $this->assertCount(1, $repo->findAll());

        /*
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
         $i="3";
         $this->assertEquals($i,"3");
         $this->assertSame(200, $client->getResponse()->getStatusCode());
         $this->assertSelectorTextContains('h3', 'Commentaire');
         $crawler->filter('.centre')->count();
         */
    }
/*
    public function testHomepageIsUp()
    {
        $user = static::createClient();
        $crawler = $user->request('GET', '/');
        $this->assertSame(1, $crawler->filter('h1')->count());
        $this->assertSame(200, $user->getResponse()->getStatusCode());
    }*/
}
