<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExitTest extends WebTestCase
{
    public function testLogin()
    {
        //logue du client admin
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'admin'
        ]);
        //$client = static::createClient();
        $crawler = $client->request('GET', '/login');
        //dump($crawler);die();
        $this->assertResponseIsSuccessful();
        //vérification de <h2>Together</h2>
        $this->assertSelectorTextContains('h2', 'Together');
    }
}
