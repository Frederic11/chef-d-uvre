<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;


class HomeControllerTest extends WebTestCase
{
    public function setUp()
    {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testPageHome()
    {
        $client = static::createClient();
        
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('Login')->form();
        $form['_username'] = 'admin';
        $form['_password'] = 'admin';
        $client->submit($form);

        $crawler = $client->request('GET', '/home');

        $this->assertSelectorExists('.ligne');
        $this->assertEquals(1, $crawler->filter('.capsule')->count());
        $this->assertSame(1, $crawler->filter('html:contains("Annonce")')->count());
        //$this->assertSelectorTextContains('h3', "Annonce");
    }
}
